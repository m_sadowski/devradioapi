# DevRadioApi

API do słuchania stacji radiowych (głównie EskaGo).

Lista stacji trzymana jest w bazie MySQL i regularnie aktualizowana (raz na dobę) za pomocą crona.

W przypadku EskaGo aplikacja działa jako proxy między serwerem EskaGo a klientem.
EskaGo jest zabezpieczone tokenem. Dodatkowo nie da się pobrać tokenu z innego ip niż tego, z którego stacja będzie słuchana.
Stąd proxy, które przesyła stream radiowy z EskaGo do słuchacza.
Słuchacz nie musi wchodzić przez oficjalne kanały EskaGo i oglądać reklam.

Aplikacja dostępna pod adresem:
https://radio.msdev.ovh

Uruchomienie:

* ustawić konfigurację w pliku .env (przykład w .env-example)
* komenda yarn
* yarn run start
