import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';
import Debug from 'debug';
import express from 'express';
import helmet from 'helmet';
import path from 'path';
import cors from 'cors';
import requestIp from 'request-ip';
import responseTime from 'response-time';
import userAgent from 'express-useragent';
import cron from 'node-cron';
// import proxy from 'express-http-proxy';
import settings from './lib/config/settings';
import index from './routes/index';
import radio from './routes/radio';
import { Log } from './lib/db/schema';
import logger from './lib/config/winston';
import RadioGroupFactory from './lib/services/radioGroupFactory';

const app = express();
const debug = Debug('devradioapi:app');
app.set('views', path.join(__dirname, 'views'));
// view engine setup
app.set('view engine', 'pug');
// uncomment after placing your favicon in /public
// app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
// app.use(logger('combined', { stream: winston.stream }));

app.use(helmet());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: false
}));

app.use(cors());
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(userAgent.express());

// Measure response time (ms), save it to db
app.use(responseTime((req, res, time) => {
  if (settings.LOG_API_REQUESTS_TO_DB) {
    if (req.originalUrl.includes('/radio') || req.originalUrl.includes('/epr')) {
      Log.create({
        ip: requestIp.getClientIp(req),
        userAgent: req.useragent.source,
        path: `${req.method} ${req.originalUrl}`,
        time
      }).then(() => {
        // logger.debug('[MAIN] Access Log created successfully in DB');
      }).catch((err) => {
        logger.error(`[MAIN] Could not create access log in DB ${err}`);
      });
    }
  }
}));

const getProxyHost = () => settings.ESKA_GO_STREAM_SERVER_URL;

// https://waw01-01.ic.smcdn.pl/t041-1.mp3?timestamp=1560081400&hash=c99ca45eb44cc61cc952a3ebb641045abdbabea7f2d848761ca7a365e35ff1af&rip=91.239.155.126&chstr=ebcd3d424945b4e65aebfab5190c908a9f8fcada3418312829912f04e24edfb5

app.use('/radio', radio);

// app.use('/epr', proxy(getProxyHost(this), {
//   proxyReqPathResolver: (req) => {
//     return new Promise(async (resolve, reject) => {
//       const eskaGo = RadioGroupFactory.get('eskago');
//       const token = await eskaGo.getAuthTokens();
//       const reqUrl = `${req.url}?${token}`;
//       logger.info(`/epr === ${reqUrl}`);

//       resolve(reqUrl);
//     });
//   }
// })); // EskaGo Proxy
app.use('/epr', (req, res, next) => {
  const reqUrl = `${getProxyHost()}${req.url}`;
  res.redirect(reqUrl);
}); // EskaGo Proxy
app.use('/', index);

// Catch 404 and forward to error handler
app.use((req, res, next) => {
  const err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
/* eslint no-unused-vars: 0 */
app.use((err, req, res, next) => {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};
  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

// Handle uncaughtException
process.on('uncaughtException', (err) => {
  debug('Caught exception: %j', err);
  process.exit(1);
});

// Cron pobierania listy stacji
cron.schedule(settings.STATION_LIST_CRON, async () => {
  try {
    await RadioGroupFactory.saveAllStations();
    logger.info('[MAIN] Cron - save all stations.');
  } catch (err) {
    logger.error(`[MAIN] Cron - save all stations error. ${err}`);
  }
});

logger.info('[MAIN] Application start.');

if (settings.GET_LIST_ON_START) {
  RadioGroupFactory.saveAllStations().then(() => {
    logger.info('[MAIN] Get stations list on start.');
  }).catch((err) => {
    logger.error(`[MAIN] Get stations list on start error. ${err}`);
  });
}

export default app;
