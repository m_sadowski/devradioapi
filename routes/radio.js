import express from 'express';
import requestIp from 'request-ip';
import RadioGroupFactory from '../lib/services/radioGroupFactory';
import settings from '../lib/config/settings';

const router = express.Router();
const authKey = settings.ADMIN_AUTH_KEY;

router.get('/', async (req, res) => {
  try {
    const result = await RadioGroupFactory.getAllStations();
    res.send(result);
  } catch (err) {
    res.status(500).send(err);
  }
});

router.get('/m3u', async (req, res) => {
  try {
    const groups = RadioGroupFactory.getGroups();
    const result = await RadioGroupFactory.getAllStations();
    let m3u = '#EXTM3U</br>';
    groups.forEach((group) => {
      const groupStations = result[group];
      if (groupStations && groupStations.length > 0) {
        groupStations.forEach((station) => {
          const stationUrl = `${settings.APP_URL}/radio/${group}/${station.name}/listen`;
          m3u += `#EXTINF:-1,${station.full} (${group})</br>${stationUrl}</br>`;
        });
      }
    });
    res.send(m3u);
  } catch (err) {
    res.status(500).send(err);
  }
});

router.get('/admin/savedb', async (req, res) => {
  const auth = req.query.auth;
  const config = { requestIp: requestIp.getClientIp(req), getToken: false };
  if (auth !== authKey) {
    res.send('unauthorized');
    return;
  }
  try {
    await RadioGroupFactory.saveAllStations(config);
    res.send('ok');
  } catch (err) {
    res.status(500).send(err);
  }
});

router.get('/admin/gettoken', async (req, res) => {
  const radio = RadioGroupFactory.get('eskago');
  const auth = req.query.auth;
  if (auth !== authKey) {
    res.send('unauthorized');
    return;
  }
  try {
    await radio.getToken();
    res.send('ok');
  } catch (err) {
    res.status(500).send(err);
  }
});

router.get('/search', (req, res) => {
  let { name, group } = req.query;
  name = name.toString().toLowerCase();
  if (group) {
    group = group.toString().toLowerCase();
  }

  RadioGroupFactory.findStations({ name, group }).then((list) => {
    res.send(list);
  });
});

router.get('/:group', (req, res) => {
  let { group } = req.params;
  group = group.toString().toLowerCase();
  const config = { requestIp: requestIp.getClientIp(req) };

  const radio = RadioGroupFactory.get(group, config);
  radio.getList().then((list) => {
    res.send(list);
  });
});

router.get('/:group/:name/:action?', (req, res) => {
  // action = listen | data
  // group = eskago | sport | pr
  // name = vox-fm | nauka | ...

  let { action = 'data', group } = req.params;
  const full = req.query.full === '1' ? 1 : 0; // 1 - get full data for station
  const name = req.params.name;
  const aac = req.query.aac === '1';

  action = action.toString().toLowerCase();
  group = group.toString().toLowerCase();
  // const config = { requestIp: requestIp.getClientIp(req), getToken: true };

  // const radio = RadioGroupFactory.get(group, config);

  // radio
  //   .getStation(name, { full })
  //   .then((stationData) => {
  //     if (action === 'data') {
  //       res.send(stationData);
  //     } else if (action === 'listen') {
  //       if (aac === true) {
  //         res.redirect(stationData.aacUrl);
  //       } else {
  //         res.redirect(stationData.url);
  //       }
  //     }
  //   })
  //   .catch((err) => {
  //     res.status(500).send(err);
  //   });

  const config = { requestIp: '', getToken: false };

  const radio = RadioGroupFactory.get(group, config);

  radio
    .getStation(name, { full })
    .then((stationData) => {
      if (action === 'data') {
        res.send(stationData);
      } else if (action === 'listen') {
        if (aac === true) {
          res.redirect(stationData.aacUrl);
        } else {
          res.redirect(stationData.url);
        }
      }
    })
    .catch((err) => {
      res.status(500).send(err);
    });
});

export default router;
