import express from 'express';
import version from '../lib/config/version';
import RadioGroupFactory from '../lib/services/radioGroupFactory';

const router = express.Router();

/* GET index page. */
router.get('/', async (req, res) => {
  const stations = await RadioGroupFactory.getAllStations();
  res.render('index', {
    title: 'MSDEV Radio API',
    version,
    stations
  });
});

export default router;
