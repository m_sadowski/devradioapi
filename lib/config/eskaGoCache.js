import NodeCache from 'node-cache';
import settings from './settings';

const eskaGoCache = new NodeCache({
  stdTTL: settings.ESKA_GO_TOKEN_TTL,
  checkperiod: settings.ESKA_GO_TOKEN_TTL
});

export default eskaGoCache;
