export default {
  NODE_ENV: process.env.NODE_ENV || 'development',
  PORT: process.env.PORT || 3000,
  APP_URL: process.env.APP_URL || 'http://localhost:3030',

  DB_URL: process.env.DB_URL || 'localhost',
  DB_NAME: process.env.DB_NAME || 'radio',
  DB_USER: process.env.DB_USER || 'root',
  DB_PASS: process.env.DB_PASS || '',

  LOG_API_REQUESTS_TO_DB: process.env.LOG_API_REQUESTS_TO_DB > 0,

  GET_LIST_ON_START: process.env.GET_LIST_ON_START > 0,

  GET_TOKEN_ON_START: process.env.GET_TOKEN_ON_START > 0,

  ADMIN_AUTH_KEY: process.env.ADMIN_AUTH_KEY,

  ESKA_GO_TOKEN_TTL: process.env.ESKA_GO_TOKEN_TTL || 300,

  STATION_LIST_CRON: process.env.STATION_LIST_CRON || '0 0 * * *',

  ESKA_GO_STREAM_SERVER_URL: process.env.ESKA_GO_STREAM_SERVER_URL || 'https://waw01-04.ic.smcdn.pl'
};
