import Sequelize from 'sequelize';
import logger from '../config/winston';
import settings from '../config/settings';

// db schema (sequelize)
const sequelize = new Sequelize(settings.DB_NAME, settings.DB_USER, settings.DB_PASS, {
  host: settings.DB_URL,
  dialect: 'mysql',

  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000
  },

  // http://docs.sequelizejs.com/manual/tutorial/querying.html#operators
  operatorsAliases: false,
  logging: false
});

sequelize
  .authenticate()
  .then(() => {
    logger.info('[DB] Connection to MySQL established successfully.');
  })
  .catch((err) => {
    logger.error(`[DB] Unable to connect to database. ${err}`);
  });

const Station = sequelize.define('station', {
  group: { type: Sequelize.STRING, max: 30, primaryKey: true },
  name: { type: Sequelize.STRING, max: 50, primaryKey: true },
  full: { type: Sequelize.STRING, max: 100, allowNull: false },
  url: { type: Sequelize.STRING, max: 100, allowNull: false },
  img: { type: Sequelize.STRING, max: 100, allowNull: true },
  bg: { type: Sequelize.STRING, max: 10, allowNull: true },
  counter: { type: Sequelize.INTEGER, allowNull: false }
}, {
  timestamps: true
});

const Log = sequelize.define('log', {
  ip: { type: Sequelize.STRING },
  path: { type: Sequelize.STRING },
  time: { type: Sequelize.INTEGER },
  userAgent: { type: Sequelize.STRING }
}, {
  timestamps: true,
  updatedAt: false,
});

sequelize.sync()
  .then(() => {
    logger.info('[DB] Sequelize synced.');
  })
  .catch((err) => {
    logger.error(`[DB] Sequelize sync error. ${err}`);
  });

export { Station, Log };
