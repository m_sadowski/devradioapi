

class ApiError extends Error {
  constructor(message) {
    super(message);
    this.name = this.constructor.name; // Nazwa bledu jest taka jak nazwa klasy
    Error.captureStackTrace(this, this.constructor);
  }
}

class NotImplementedError extends Error {
  constructor(methodName, className) {
    super(`Method ${methodName} not implemented.`);
    this.data = { methodName, className };
  }
}

export { ApiError, NotImplementedError };

