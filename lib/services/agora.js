import RadioGroup from './radioGroup';

class Agora extends RadioGroup {
  constructor() {
    super('agora');
  }

  async getListSrc() {
    return [
      {
        name: 'tok-fm',
        full: 'TOK FM',
        url: 'http://wroclaw.radio.pionier.net.pl:8000/pl/tuba10-1.mp3',
        img: 'https://bis.gazeta.pl/im/2/22623/m22623812.png'
      }
    ];
    // return new Promise((resolve, reject) => {
    //   if (this) {
    //     resolve([
    //       {
    //         name: 'tok-fm',
    //         full: 'TOK FM',
    //         url: 'http://wroclaw.radio.pionier.net.pl:8000/pl/tuba10-1.mp3',
    //         img: 'https://bis.gazeta.pl/im/2/22623/m22623812.png'
    //       }
    //     ]);
    //   } else {
    //     reject(null);
    //   }
    // });
  }
}

export default Agora;

// `http://wroclaw.radio.pionier.net.pl:8000/pl/tuba10-1.mp3?cache=1557222374_redir__e1`
// chyba timestamp do cache wstawiony, ale nie jest konieczny
