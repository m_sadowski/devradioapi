import axios from 'axios';
import getUrls from 'get-urls';
import _ from 'underscore';
import cheerio from 'cheerio';
import logger from '../config/winston';
import settings from '../config/settings';
import eskaGoCache from '../config/eskaGoCache';
import RadioGroup from './radioGroup';
import { EskaGoToken } from '../db/schema';

class EskaGo extends RadioGroup {
  constructor(config) {
    super('eskago', config);
    this.stationMainUrl = 'https://eskago.pl/radio/';
    this.stationName = 'vox-fm';
    // this.stationListUrl = `${this.stationMainUrl}${this.stationName}`;
    this.stationListUrl = `${this.stationMainUrl}`;
  }

  getStationFullName(name) {
    // myślnik zastąpic spacją
    // fm na FM
    // pierwsza litera kazdego słowa na uppercase
    // do replace trzeba podac regexpa z flagą 'g' (/g)
    const fullName = name
      .replace(new RegExp('-', 'g'), ' ')
      .replace(new RegExp('fm', 'g'), 'FM')
      .replace(/\b(\w)/g, c => c.toUpperCase());

    return fullName;
  }

  getStationImageInfo() {
    // na początek jedno logo eskaGo dla wszystkich
    return { img: `${settings.APP_URL}/img/eskago2.png`, bg: '#ffffff' };
  }

  getStationsFromUrls(urls) {
    const stations = [];
    urls.forEach((u) => {
      if (u.indexOf(this.stationMainUrl) >= 0) {
        const n = u.substr(this.stationMainUrl.length);
        stations.push({
          name: n,
          url: u
        });
      }
    });
    return stations;
  }

  async getListSrc() {
    const response = await axios.get(this.stationListUrl);
    const urls = Array.from(
      getUrls(response.data, { stripFragment: false })
    );
    const stations = _.uniq(this.getStationsFromUrls(urls), item => item.name);
    return stations;
    // return new Promise((resolve, reject) => {
    //   axios
    //     .get(this.stationListUrl)
    //     .then((response) => {
    //       const urls = Array.from(
    //         getUrls(response.data, { stripFragment: false })
    //       );
    //       const stations = _.uniq(this.getStationsFromUrls(urls), item => item.name);
    //       resolve(stations);
    //     })
    //     .catch((error) => {
    //       reject(error);
    //     });
    // });
  }

  async getAuthTokens() {
    const tokenFromCache = eskaGoCache.get('token');
    if (tokenFromCache !== undefined) {
      // logger.info('Token from CACHE.');
      // logger.info(tokenFromCache);
      return tokenFromCache;
    }
    const response = await axios.get(`${this.stationMainUrl}${this.stationName}`);
    const $ = cheerio.load(response.data);
    const token = $('#icsu').attr('value');
    eskaGoCache.set('token', token);
    // logger.info(`Token: ${token}`);
    return token;
    // return new Promise((resolve, reject) => {
    //   // logger.info('Get token2');
    //   const tokenFromCache = eskaGoCache.get('token');
    //   if (tokenFromCache !== undefined) {
    //     // logger.info('Token from CACHE.');
    //     // logger.info(tokenFromCache);
    //     resolve(tokenFromCache);
    //   } else {
    //     axios
    //       .get(`${this.stationMainUrl}${this.stationName}`)
    //       .then((response) => {
    //         const $ = cheerio.load(response.data);
    //         const token = $('#icsu').attr('value');
    //         eskaGoCache.set('token', token);
    //         // logger.info(`Token: ${token}`);
    //         resolve(token);
    //       })
    //       .catch((error) => {
    //         reject(error);
    //       });
    //   }
    // });
  }

  async getStationSrc({ name }) {
    const response = await axios.get(`${this.stationMainUrl}${name}`);
    const urls = Array.from(
      getUrls(response.data, { stripFragment: false })
    );
    let link = _.find(
      urls,
      it =>
        // it.indexOf('waw.ic.smcdn.pl') >= 0 && it.indexOf('replace') < 0
        it.includes('ic.smcdn.pl')
    );
    if (link) {
      const result = {
        name,
        full: this.getStationFullName(name),
        ...this.getStationImageInfo(name)
      };
      link = link.replace("';", '');
      if (this.getConfigGetToken() === true) {
        // wyciagnac auth key (ciag 64 znakow alfanumerycznych)
        const authArr = response.data.toString().match('[a-z0-9]{64}');
        if (authArr.length === 0) {
          throw new Error('Cannot get eskaGo auth key.');
        }
        const auth = authArr[0];
        const requestIp = await this.getConfigRequestIp();
        result.token = await this.getAuthTokens(requestIp, auth);
      }
      result.url = link.split('?')[0].replace('aac', 'mp3');
      if (link.indexOf('aac') >= 0) result.aacUrl = link;

      return result;
    }
    throw new Error(`Cannot get eskaGo ${name}`);
    // return new Promise((resolve, reject) => {
    //   axios
    //     .get(`${this.stationMainUrl}${name}`)
    //     .then(async (response) => {
    //       const urls = Array.from(
    //         getUrls(response.data, { stripFragment: false })
    //       );
    //       let link = _.find(
    //         urls,
    //         it =>
    //           it.indexOf('waw.ic.smcdn.pl') >= 0 && it.indexOf('replace') < 0
    //       );
    //       if (link) {
    //         const result = {
    //           name,
    //           full: this.getStationFullName(name),
    //           ...this.getStationImageInfo(name)
    //         };
    //         link = link.replace("';", '');
    //         if (this.getConfigGetToken() === true) {
    //           // wyciagnac auth key (ciag 64 znakow alfanumerycznych)
    //           const authArr = response.data.toString().match('[a-z0-9]{64}');
    //           if (authArr.length === 0) {
    //             reject(new Error('Cannot get eskaGo auth key.'));
    //             return;
    //           }
    //           const auth = authArr[0];
    //           const requestIp = await this.getConfigRequestIp();
    //           result.token = await this.getAuthTokens(requestIp, auth);
    //         }
    //         result.url = link.split('?')[0].replace('aac', 'mp3');
    //         if (link.indexOf('aac') >= 0) result.aacUrl = link;

    //         resolve(result);
    //       }
    //       else {
    //         reject(new Error(`Cannot get eskaGo ${name}`));
    //       }
    //     })
    //     .catch((error) => {
    //       reject(error);
    //     });
    // });
  }

  async getStation(name, config) {
    const station = await super.getStation(name, config);
    // Token doklejany w main.js w roucie /epr (przy faktycznym requescie do waw.ic.smcdn.pl)
    station.url = station.url.replace('https://waw.ic.smcdn.pl', `${settings.APP_URL}/epr`);
    return station;
    // return new Promise(async (resolve, reject) => {
    //   try {
    //     const station = await super.getStation(name, config);
    //     // Token doklejany w main.js w roucie /epr (przy faktycznym requescie do waw.ic.smcdn.pl)
    //     station.url = station.url.replace('https://waw.ic.smcdn.pl', `${settings.APP_URL}/epr`);
    //     resolve(station);
    //   } catch (err) {
    //     reject(err);
    //   }
    // });
  }

  async getToken() {
    const station = await this.getStationSrc(this.stationName);
    const hash = station.token.hash;
    const timestamp = station.token.timestamp;

    const token = await EskaGoToken.create({ hash, timestamp });
    const t = token.get({ plain: true });
    logger.info(`[EskaGo] Get token. Hash: ${t.hash}, timestamp: ${t.timestamp}`);
    return t;
  }
}

export default EskaGo;
