import RadioGroup from './radioGroup';

class Eurozet extends RadioGroup {
  constructor() {
    super('eurozet');
  }

  async getListSrc() {
    return [
      {
        name: 'zet',
        full: 'Radio ZET',
        url: 'https://n-5-15.dcs.redcdn.pl/sc/o2/Eurozet/live/audio.livx?audio=5',
        img: 'https://gfx.radiozet.pl/extension/radiozet/design/standard/images/layout/logo-zet.svg',
        bg: '#B00320'
      },
      {
        name: 'meloradio',
        full: 'Meloradio',
        url: 'https://n-0-117.dcs.redcdn.pl/sc/o2/Eurozet/live/meloradio.livx?audio=5',
        img: 'https://gfx.meloradio.pl/design/meloradio/images/layout/logo.png'
      },
      {
        name: 'antyradio',
        full: 'Antyradio',
        url: 'https://n-5-10.dcs.redcdn.pl/sc/o2/Eurozet/live/antyradio.livx?audio=5',
        img: 'https://gfx.antyradio.pl/design/antyradio/images/layout/logo.png',
        bg: '#333'
      },
      {
        name: 'chillizet',
        full: 'Chilli ZET',
        url: 'https://n-1-1.dcs.redcdn.pl/sc/o2/Eurozet/live/chillizet.livx?audio=5',
        img: 'https://gfx.chillizet.pl/design/chillizet/images/logo.svg'
      }
    ];
    // return new Promise((resolve, reject) => {
    //   if (this) {
    //     resolve([
    //       {
    //         name: 'zet',
    //         full: 'Radio ZET',
    //         url: 'https://n-5-15.dcs.redcdn.pl/sc/o2/Eurozet/live/audio.livx?audio=5',
    //         img: 'https://gfx.radiozet.pl/extension/radiozet/design/standard/images/layout/logo-zet.svg',
    //         bg: '#B00320'
    //       },
    //       {
    //         name: 'meloradio',
    //         full: 'Meloradio',
    //         url: 'https://n-0-117.dcs.redcdn.pl/sc/o2/Eurozet/live/meloradio.livx?audio=5',
    //         img: 'https://gfx.meloradio.pl/design/meloradio/images/layout/logo.png'
    //       },
    //       {
    //         name: 'antyradio',
    //         full: 'Antyradio',
    //         url: 'https://n-5-10.dcs.redcdn.pl/sc/o2/Eurozet/live/antyradio.livx?audio=5',
    //         img: 'https://gfx.antyradio.pl/design/antyradio/images/layout/logo.png',
    //         bg: '#333'
    //       },
    //       {
    //         name: 'chillizet',
    //         full: 'Chilli ZET',
    //         url: 'https://n-1-1.dcs.redcdn.pl/sc/o2/Eurozet/live/chillizet.livx?audio=5',
    //         img: 'https://gfx.chillizet.pl/design/chillizet/images/logo.svg'
    //       }
    //     ]);
    //   } else {
    //     reject(null);
    //   }
    // });
  }
}

export default Eurozet;
