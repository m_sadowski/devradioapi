import { Op } from 'sequelize';
import logger from '../config/winston';
import EskaGo from './eskago';
import Sport from './sport';
import Eurozet from './eurozet';
import Agora from './agora';
import PolishRadio from './polishRadio';
import Foreign from './foreign';
import RmfOn from './rmfon';
import Polsat from './polsat';
import { Station } from '../db/schema';

class RadioGroupFactory {
  static getGroups() {
    return ['eskago', 'sport', 'pr', 'eurozet', 'agora', 'foreign', 'rmfon', 'polsat'];
  }

  static get(group, config) {
    const gr = group.toString().toLowerCase();
    let radio = null;
    switch (gr) {
      case 'eskago':
        radio = new EskaGo(config);
        break;
      case 'sport':
        radio = new Sport(config);
        break;
      case 'pr':
        radio = new PolishRadio(config);
        break;
      case 'eurozet':
        radio = new Eurozet(config);
        break;
      case 'agora':
        radio = new Agora(config);
        break;
      case 'foreign':
        radio = new Foreign(config);
        break;
      case 'rmfon':
        radio = new RmfOn(config);
        break;
      case 'polsat':
        radio = new Polsat(config);
        break;
      default:
        radio = null;
    }
    return radio;
  }

  static getAllStations(config) {
    return new Promise((resolve, reject) => {
      const groups = RadioGroupFactory.getGroups();
      const promises = [];
      const result = {};
      groups.forEach((group) => {
        const radio = RadioGroupFactory.get(group, config);
        promises.push(radio.getList());
      });
      Promise.all(promises).then((arr) => {
        arr.forEach((it, index) => {
          result[groups[index]] = it;
        });
        resolve(result);
      }).catch((err) => {
        reject(err);
      });
    });
  }

  static findStations(query = {}) {
    // query.name, query.group
    return new Promise(async (resolve, reject) => {
      if (!query.name) {
        throw new Error('Name is required for finding station.');
      }
      try {
        const whereClause = {
          // name: { [Op.contains]: query.name },
          name: { [Op.like]: `%${query.name}%` }
          // group: (query.group || undefined)
        };
        const found = await Station.findAll({
          where: whereClause,
          attributes: [
            'group', 'name', 'full', 'url', 'img', 'bg'
          ]
        });
        resolve(found);
      } catch (err) {
        reject(err);
      }
    });
  }

  static saveAllStations(config) {
    return new Promise((resolve, reject) => {
      const groups = RadioGroupFactory.getGroups();
      const promises = [];
      groups.forEach((group) => {
        const radio = RadioGroupFactory.get(group, config);
        promises.push(radio.saveStations());
      });

      Promise.all(promises).then(() => {
        resolve();
      }).catch((err) => {
        logger.error(`[RadioGroupFactory] Save all stations error. ${err}`);
        reject(err);
      });
    });
  }
}

export default RadioGroupFactory;
