import RadioGroup from './radioGroup';

class Sport extends RadioGroup {
  constructor() {
    super('sport');
  }

  async getListSrc() {
    return [{
      name: 'weszlo-fm',
      full: 'Weszlo FM',
      url: 'http://streams.radio.co/s7d70a7895/listen',
      img: 'https://weszlo.fm/wp-content/themes/weszlofm/images/logo.svg',
      bg: '#000000'
    }];
    // return new Promise((resolve, reject) => {
    //   if (this) {
    //     resolve([{
    //       name: 'weszlo-fm',
    //       full: 'Weszlo FM',
    //       url: 'http://streams.radio.co/s7d70a7895/listen',
    //       img: 'https://weszlo.fm/wp-content/themes/weszlofm/images/logo.svg',
    //       bg: '#000000'
    //     }]);
    //   } else {
    //     reject(null);
    //   }
    // });
  }
}

export default Sport;
