import axios from 'axios';
import convert from 'xml-js';
import RadioGroup from './radioGroup';

class RmfOn extends RadioGroup {
  constructor() {
    super('rmfon');
    this.listUrl = 'https://www.rmfon.pl/json/stations.txt';
  }

  getStationImageInfo() {
    return { img: 'https://www.rmfon.pl/img/logo.png', bg: '#fc0' };
  }

  async getListSrc() {
    const response = await axios.get(this.listUrl);
    const stations = response.data.map(station => ({
      name: station.slug,
      full: station.name,
      id: station.id
    }));
    return stations;

    // return new Promise((resolve, reject) => {
    //   axios
    //     .get(this.listUrl)
    //     .then((response) => {
    //       // eslint-disable-next-line arrow-body-style
    //       const stations = response.data.map((station) => {
    //         return {
    //           name: station.slug,
    //           full: station.name,
    //           id: station.id
    //         };
    //       });
    //       resolve(stations);
    //     })
    //     .catch((error) => {
    //       reject(error);
    //     });
    // });
  }

  async getStationSrc({ name, full, id }) {
    const response = await axios.get(`https://www.rmfon.pl/stacje/flash_aac_${id}.xml.txt`);
    const station = convert.xml2js(response.data, { compact: true, spaces: 2 });
    // eslint-disable-next-line no-underscore-dangle
    const url = station.xml.playlistMp3.item_mp3[0]._text;
    // eslint-disable-next-line no-underscore-dangle
    const result = {
      name,
      full,
      url,
      ...this.getStationImageInfo(name)
    };
    return result;
    // return new Promise((resolve, reject) => {
    //   axios
    //     .get(`https://www.rmfon.pl/stacje/flash_aac_${id}.xml.txt`)
    //     .then(async (response) => {
    //       const station = convert.xml2js(response.data, { compact: true, spaces: 2 });
    //       // eslint-disable-next-line no-underscore-dangle
    //       const url = station.xml.playlistMp3.item_mp3[0]._text;
    //       // eslint-disable-next-line no-underscore-dangle
    //       resolve({
    //         name,
    //         full,
    //         url,
    //         ...this.getStationImageInfo(name)
    //       });
    //     })
    //     .catch((error) => {
    //       reject(error);
    //     });
    // });
  }
}

export default RmfOn;
