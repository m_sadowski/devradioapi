import RadioGroup from './radioGroup';

class Foreign extends RadioGroup {
  constructor() {
    super('foreign');
  }

  async getListSrc() {
    return [
      {
        name: 'racyja',
        full: 'Radio Racyja',
        url: 'http://air.racyja.com:8000/racja256',
        img: 'https://www.racyja.com/wp-content/themes/racyja/img/logo-white.png',
        bg: '#d1050c'
      }
    ];
    // return new Promise((resolve, reject) => {
    //   if (this) {
    //     resolve([
    //       {
    //         name: 'racyja',
    //         full: 'Radio Racyja',
    //         url: 'http://air.racyja.com:8000/racja256',
    //         img: 'https://www.racyja.com/wp-content/themes/racyja/img/logo-white.png',
    //         bg: '#d1050c'
    //       }
    //     ]);
    //   } else {
    //     reject(null);
    //   }
    // });
  }
}

export default Foreign;
