import _ from 'underscore';
import Sequelize, { Op } from 'sequelize';
import publicIp from 'public-ip';
import logger from '../config/winston';
import { Station } from '../db/schema';
import { NotImplementedError } from '../apiError';

class RadioGroup {
  /**
   * RadioGroup constructor
   * @param {*} groupName Name of radio group (i.e. eskago, pr, ...)
   * @param {*} config Configuration { requestIp }
   */
  constructor(groupName, config = { requestIp: '::1', getToken: false }) {
    if (!groupName) {
      throw new Error('RadioGroup - name not provided');
    }
    this.groupName = groupName;
    this.config = config;
  }

  /**
   * Get external ip if localhost (for eskaGo localhost testing)
   */
  async getConfigRequestIp() {
    if (this.config.requestIp.includes('::1')) {
      this.config.requestIp = await publicIp.v4();
    }
    logger.info(`Request IP: ${this.config.requestIp}`);
    return this.config.requestIp;
    // return new Promise(async (resolve) => {
    //   if (this.config.requestIp.includes('::1')) {
    //     this.config.requestIp = await publicIp.v4();
    //   }
    //   logger.info(`Request IP: ${this.config.requestIp}`);
    //   resolve(this.config.requestIp);
    // });
  }

  getConfigGetToken() {
    return this.config.getToken;
  }

  /**
   * Get station list from source
   */
  async getListSrc() {
    throw new NotImplementedError(this.getListSrc.name, this.name);
    // return new Promise((resolve, reject) => {
    //   reject(new NotImplementedError(this.getListSrc.name, this.name));
    // });
  }

  /**
   * Get station list from database
   */
  async getList() {
    let list = await Station.findAll({
      where: {
        group: this.groupName,
        updatedAt: {
          // eslint-disable-next-line no-mixed-operators
          [Op.gt]: new Date(new Date() - 48 * 60 * 60 * 1000) // aktualizowane w ostatnich 48h
        }
      },
      order: [
        ['name']
      ],
      attributes: [
        'group', 'name', 'full', 'img', 'bg'
      ]
    });
    list = list.map(st => st.get({ plain: true }));
    return list;
    // return new Promise(async (resolve, reject) => {
    //   try {
    //     let list = await Station.findAll({
    //       where: {
    //         group: this.groupName,
    //         updatedAt: {
    //           // eslint-disable-next-line no-mixed-operators
    //           [Op.gt]: new Date(new Date() - 48 * 60 * 60 * 1000) // aktualizowane w ostatnich 48h
    //         }
    //       },
    //       order: [
    //         ['name']
    //       ],
    //       attributes: [
    //         'group', 'name', 'full', 'img', 'bg'
    //       ]
    //     });
    //     list = list.map(st => st.get({ plain: true }));
    //     resolve(list);
    //   } catch (err) {
    //     reject(err);
    //   }
    // });
  }

  async getStationSrc({ name }) {
    const list = await this.getListSrc();
    const station = _.find(list, it => it.name === name);
    return station;
    // return new Promise(async (resolve, reject) => {
    //   const list = await this.getListSrc();
    //   const station = _.find(list, it => it.name === name);
    //   if (station) resolve(station);
    //   else reject(null);
    // });
  }

  /**
   * Get station data from database
   * @param {*} name Station name
   */
  async getStation(name, config = { full: 0 }) {
    let attr = ['group', 'name', 'full', 'url'];
    if (config.full > 0) {
      attr = attr.concat(['img', 'bg']);
    }
    const found = await Station.findOne({
      where: {
        group: this.groupName,
        name
      },
      attributes: attr
    });
    return found;
    // return new Promise(async (resolve, reject) => {
    //   try {
    //     let attr = ['group', 'name', 'full', 'url'];
    //     if (config.full > 0) {
    //       attr = attr.concat(['img', 'bg']);
    //     }
    //     const found = await Station.findOne({
    //       where: {
    //         group: this.groupName,
    //         name
    //       },
    //       attributes: attr
    //     });
    //     resolve(found);
    //   } catch (err) {
    //     reject(err);
    //   }
    // });
  }

  async saveStation(st) {
    // st.name, st.url
    return new Promise((resolve, reject) => {
      const findCondition = { group: this.groupName, name: st.name };
      Station
        .findOrCreate({
          where: findCondition,
          defaults: { full: st.full, url: st.url, img: st.img, bg: st.bg, counter: 1 }
        })
        .spread((station, created) => {
          if (!created) {
            try {
              station.update({
                full: st.full,
                url: st.url,
                img: st.img,
                bg: st.bg,
                counter: Sequelize.literal('counter + 1')
              }).then(() => {
                resolve(station.get({
                  plain: true
                }));
              }).catch((err) => {
                reject(err);
              });
            } catch (err) {
              reject(err);
            }
          } else {
            resolve(station.get({
              plain: true
            }));
          }
        });
    });
  }

  async asyncForEach(array, callback) {
    if (this) {
      // eslint-disable-next-line no-plusplus
      for (let index = 0; index < array.length; index++) {
        // eslint-disable-next-line no-await-in-loop
        await callback(array[index], index, array);
      }
    }
  }

  saveStations() {
    return new Promise(async (resolve, reject) => {
      const list = await this.getListSrc();
      const stations = [];
      const startGetList = async () => {
        await this.asyncForEach(list, async (st) => {
          try {
            const x = await this.getStationSrc(st);
            stations.push(x);
          } catch (err) {
            // throw (err);
            console.log(err);
          }
        });
      };

      try {
        await startGetList();
      } catch (err) {
        reject(err);
      }

      let savedCount = 0;

      const startSaveList = async () => {
        await this.asyncForEach(stations, async (st) => {
          try {
            await this.saveStation(st);
            savedCount += 1;
          } catch (err) {
            throw (err);
          }
        });
      };
      try {
        await startSaveList();
      } catch (err) {
        reject(err);
      }
      logger.info(`[RadioGroup] ${this.groupName}. ${savedCount} stations saved.`);
      resolve(savedCount);
    });
  }
}

export default RadioGroup;
