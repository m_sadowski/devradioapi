import RadioGroup from './radioGroup';

class Polsat extends RadioGroup {
  constructor() {
    super('polsat');
  }

  async getListSrc() {
    return [
      {
        name: 'muzo-fm',
        full: 'Muzo FM',
        url: 'https://stream4.nadaje.com/muzo',
        img: 'https://www.muzo.fm/templates/muzo/gfx/logo.svg'
      }
    ];
    // return new Promise((resolve, reject) => {
    //   if (this) {
    //     resolve([
    //       {
    //         name: 'muzo-fm',
    //         full: 'Muzo FM',
    //         url: 'https://stream4.nadaje.com/muzo',
    //         img: 'https://www.muzo.fm/templates/muzo/gfx/logo.svg'
    //       }
    //     ]);
    //   } else {
    //     reject(null);
    //   }
    // });
  }
}

export default Polsat;