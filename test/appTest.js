import chai from 'chai';
import chaiHttp from 'chai-http';
import app from '../app';
import EskaGo from '../lib/services/eskago';

chai.should();
chai.use(chaiHttp);

/* Test the /GET route */
describe('app index route', () => {
  it('it should GET /', (done) => {
    chai
      .request(app)
      .get('/')
      .end((err, res) => {
        res.should.have.status(200);
        done();
      });
  });

  it('it should handle 404 error', (done) => {
    chai
      .request(app)
      .get('/notExist')
      .end((err, res) => {
        res.should.have.status(404);
        done();
      });
  });
});

// describe('radio route', () => {
//   let eskaGoList = [];

//   it('eskago list', (done) => {
//     chai
//       .request(app)
//       .get('/radio/eskago')
//       .end((err, res) => {
//         res.should.have.status(200);
//         res.body.should.be.a('array');
//         res.body.length.should.be.above(0);
//         // res.body.forEach((el) => {
//         //   el.should.have.property('name');
//         //   el.should.have
//         //     .property('url')
//         //     .include('http://eskago.pl/radio/');
//         //   el.should.have.property('aac');
//         // });
//         eskaGoList = res.body;
//         done();
//       });
//   });

//   it('eskago get station', (done) => {
//     const first = eskaGoList[0];
//     // console.log('eska list', eskaGoList);
//     // done();

//     chai
//       .request(app)
//       .get(`/radio/eskago/${first.name}`)
//       .end((err, res) => {
//         res.should.have.status(200);
//         res.body.should.be.a('object');
//         res.body.should.have.property('name');
//         // res.body.should.have.property('url').include('http://waw.ic.smcdn.pl/');
//         done();
//       });
//   });
// });

describe('eskago service', () => {
  // eslint-disable-next-line no-unused-vars
  let service = null;
  beforeEach(() => {
    service = new EskaGo();
  });
  it('eskago list', async () => {
    const list = await service.getListSrc();
    // console.log(list);
  });
  it('eskago alternative link', async () => {
    const list = await service.getStationSrc({ name: 'alternative' });
    console.log(list);
  });
});
